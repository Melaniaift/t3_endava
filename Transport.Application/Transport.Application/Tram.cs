﻿namespace Transport.Application
{
    class Tram : Vehicle
    {
        public override decimal fee()
        {
            return 2;

        }
        public override decimal vehicleNr()
        {
            return 1;
        }
    }
}
