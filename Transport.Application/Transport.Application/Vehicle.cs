﻿namespace Transport.Application
{
    public abstract class Vehicle
    {
        public abstract decimal vehicleNr();
        public abstract decimal fee();
}
}
