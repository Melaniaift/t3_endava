﻿namespace Transport.Application
{
    public class Routes : IRuta1, IRuta2
    {
        void IRuta1.sayRoute()
        {
            System.Console.WriteLine("Statia1-Statia2-Statia3");
        }
        void IRuta2.sayRoute()
        {
            System.Console.WriteLine("Statia4-Statia5-Statia6");
        }
    }
}
