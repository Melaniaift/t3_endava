﻿using System;

namespace Transport.Application
{
    class Program
    {
        static void Main(string[] args)
        {
            var tramvai1 = new Tram();
            System.Console.Write(tramvai1.vehicleNr());

            System.Console.Write($"Price of the ticket: {tramvai1.fee()} RON");

            Routes sample = new Routes();
            IRuta1 sample1 = sample;
            IRuta2 sample2 = sample;
            
            sample1.sayRoute();
            sample2.sayRoute();
        }
    }
}
